# README #

This is a repo for testing Git LFS and its Ansible role.

### Tracking ###

Use `git lfs track` to see patterns Git LFS is currently handling, or `git lfs track <pattern>` to add a new pattern. 
Patterns are file types — `git lfs track "*.png"` — or paths — `git lfs track "some/path/goes/here"`).

Patterns are added to .gitattributes, which should be pushed as normal — `git add .gitattributes`, commit, push.